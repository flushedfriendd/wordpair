import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      return new MaterialApp(
        title: 'WordPair',
        home: new RandomWord()
      );
    }
}

class RandomWord extends StatefulWidget {
  @override
  GenerateRandomWords createState() => new GenerateRandomWords();
}

class GenerateRandomWords extends State<RandomWord> {

  String _wordPair;
  
  void _generateWord() {
    setState(() {
          _wordPair = WordPair.random().asCamelCase;
    });              
  }

  @override
  Widget build(BuildContext context) {
      return new Scaffold(
         appBar: AppBar(
           title: new Text('Hello'),
         ),
         body: Center(
           child: new Text('$_wordPair', style: TextStyle(fontSize: 48.5),),
         ),
         floatingActionButton: new FloatingActionButton(
           onPressed: _generateWord,
           tooltip: 'press',
           child: new Icon(Icons.add),
         ),
      );
    }
}
